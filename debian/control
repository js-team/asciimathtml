Source: asciimathtml
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Marcelo Jorge Vieira <metal@debian.org>
Build-Depends: debhelper-compat (= 13), uglifyjs
Standards-Version: 4.6.1
Homepage: https://asciimathml.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/js-team/asciimathtml
Vcs-Git: https://salsa.debian.org/js-team/asciimathtml.git
Rules-Requires-Root: no

Package: libjs-asciimathml
Architecture: all
Depends: ${misc:Depends}
Recommends: javascript-common
Multi-Arch: foreign
Description: Library to render high quality mathematical formulas in a browser
 ASCIIMathML is a set of JavaScript functions to convert ASCII math
 notation and LaTeX to Presentation MathML. Simple graphics commands are
 also translated to SVG images. ASCIIMathML is designed to be very easy
 to use and mimic to a great extent the way that one would use to type
 mathematical formulas by means like e-mail.
 .
 This makes its use especially suited for teachers and instructors of
 disciplines based on Mathematics that want to use an electronic medium
 (like, for instance, a blog or a web forum) with students and still not
 be encumbered with difficult, non-intuitive, non-standard notation for
 formulas.
